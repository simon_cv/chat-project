#include "Message.h"

Message::Message(const std::string& nameOfSender, const std::string& nameOfReciver, const std::string& msgContent, const bool messageToOtherUserOrNot)

{
    this->_msgContent = msgContent;
    this->_nameOfReciver = nameOfReciver;
    this->_nameOfSender = nameOfSender;
    this->_messageToOtherUserOrNot = messageToOtherUserOrNot;
}

//function that returns name of sender
const std::string Message::getNameOfSender() const
{
    return this->_nameOfSender;
}

//function that returns name of reciver
const std::string Message::getNameOfReciver() const
{
    return this->_nameOfReciver;
}

//function that returns the message itself
const std::string Message::getMsgContent() const
{
    return this->_msgContent;
}

//function that gets messageToOtherUserOrNot
const bool Message::getMessageToOtherUserOrNot() const
{
    return this->_messageToOtherUserOrNot;
}
