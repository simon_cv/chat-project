#pragma once
#include <iostream>
#include <map>
#include <WinSock2.h>
#include <Windows.h>
#include <thread>
#include <queue>
#include <utility>
#include "Helper.h"
#include <fstream>
#include "Message.h"
class Server
{
public:
	Server();
	~Server();
	void serve(int port);
	void parseMessagesFromQueueAndReturnResp();
	const std::string getUsers() const;
	const std::string getChatContent(const std::string& name, const std::string& otherUserName) const;
	void getMessageAndEnterToQueue(const SOCKET &clientSocket);
	void writeMsgToFileAndSend();
	void updateUserListAndAcceptLogin();
	const SOCKET findSocketByName(const std::string& name) const;
	const bool checkIfSocketConected(const SOCKET& clientSocket) const;
private:

	void acceptClient();
	void clientHandler(const SOCKET &clientSocket);
	SOCKET _serverSocket;
	std::map<SOCKET, std::string> clientSessions;
	std::queue<std::pair<std::string, Message>> messages;//the first string is the name of the user you want to send a message to and the class is the mesage itself
};

