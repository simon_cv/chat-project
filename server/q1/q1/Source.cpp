#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>
#include <thread>

int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server myServer;
		std::thread parsingThread(&Server::parseMessagesFromQueueAndReturnResp, &myServer);
		parsingThread.detach();
		myServer.serve(8876);	
		//close all sockets
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	system("PAUSE");
	return 0;
}