#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include <chrono>
#include "Helper.h"

#define LEN_ALL_USERS_LEN 5
#define LEN_OF_MSG_LEN 5
#define START_OF_AUTHOR_NAME 23
#define LEN_OF_ACTION_CODE 3
#define LEN_OF_CHAT_CONTENT_LEN 5
#define LEN_OF_LEN_PAR_USERNAME 2

std::mutex userMap;
std::mutex messageQueue;
std::condition_variable waitForMessage;

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;
		
	std::thread threadThatAcceptsClients(&Server::acceptClient,this);//making a thread that accepts clients
	threadThatAcceptsClients.join();
}


void Server::acceptClient()
{
	std::string name;

	while (true)
	{
		// this accepts the client and create a specific socket from server to this client
		// the process will not continue until a client connects to the server
		SOCKET client_socket = accept(_serverSocket, NULL, NULL);
		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);
		
		try
		{
			Helper::getMessageTypeCode(client_socket);//getting rid of the client code so getName will be universal to all requests that include user names
			name = Helper::getUserName(client_socket);
		}
		catch (std::exception& e)//in case conection was interupted freeing socket and listening to new clients
		{
			closesocket(client_socket);
			continue;
		}
		
		std::unique_lock<std::mutex> addUser(userMap);
		this->clientSessions.insert(std::pair<SOCKET, std::string>(client_socket,name));//if a new client connects adding its info to the map
		addUser.unlock();
		this->updateUserListAndAcceptLogin();//updating all users that a new user joined and accepting the login of the user

		std::thread(&Server::clientHandler, this, client_socket).detach();//creating a thread that will communicate with the client
	}
}

/*
function that handels the client
input: socket of client
output:none
*/
void Server::clientHandler(const SOCKET &clientSocket)
{
	
	std::string resp;
	
	while (true)
	{
		try
		{
			this->getMessageAndEnterToQueue(clientSocket);
		}
		catch (const std::exception& e)
		{
			std::cout << e.what();
			std::unique_lock<std::mutex> removeUser(userMap);//removing user from map if conection got lost
			this->clientSessions.erase(clientSocket);
			removeUser.unlock();
			closesocket(clientSocket);//closing the socket
			break;//killing the thread if conection got lost
		}
	}
}

/*
function that returns the user part of the server response to the response string
input: refernce to response string
output: usre part of server update message
*/
const std::string Server::getUsers() const
{
	std::string users = "";
	for (auto it = this->clientSessions.begin(); it != this->clientSessions.end(); it++)//going through all of the open clients and adding the name to the string
	{
		users += it->second;
		users += "&";
	}
	users.pop_back();//removing last & that doesnt seperate between any names
	
	return users;
}

/*
function that returns the chat content between 2 users
input: 2 users to find their chat
output: the content of their chat
*/
const std::string Server::getChatContent(const std::string& name, const std::string& otherUserName) const
{
	std::string line,content;
	std::string fileName = Helper::getFileName(name, otherUserName);

	std::ifstream myFile(fileName);
	if (!myFile.fail())//if file exists
	{
		while (std::getline(myFile, line))//reading file content
		{
			content += line;
			if(!myFile.eof())//checking if getLine reached to the end of the file, if not adding \n because it means there is \n in the message 
				content += '\n';
		}
		
	}
	myFile.close();
	return content;
}

/*
function that gets the message from the socket and parses it into the queue
input:socket of sender
output: none
*/
void Server::getMessageAndEnterToQueue(const SOCKET &clientSocket)
{
	
	if (!this->checkIfSocketConected(clientSocket))//checking if user is still conected to server
	{
		throw(std::exception("Error! user disconected, removing user\n"));
	}
	
	std::unique_lock<std::mutex> findUsername(userMap);
	std::string name = this->clientSessions.find(clientSocket)->second;
	findUsername.unlock();

	if (Helper::getMessageTypeCode(clientSocket) == MT_CLIENT_UPDATE)//entering message to queue only if client sent a message
	{
		std::string otherUsername = Helper::getUserName(clientSocket);
		std::string chatContent = this->getChatContent(name, otherUsername);
		std::string users = this->getUsers();
		int dataLen = Helper::getIntPartFromSocket(clientSocket, LEN_OF_MSG_LEN);

		if (dataLen)//sending message to other user only if message got content
		{
			std::string data = Helper::getStringPartFromSocket(clientSocket, dataLen);
			std::string msg = "&MAGSH_MESSAGE&&Author&" + name + "&DATA&" + data;//making message format
			Message msgToOtherUser(name, otherUsername, msg, true);

			std::unique_lock<std::mutex> parseMessage(messageQueue);
			this->messages.push(std::pair<std::string, Message>(otherUsername, msgToOtherUser));//pushing the message into the queue
			parseMessage.unlock();
			waitForMessage.notify_all();//alerting the thread that sends message that a message was pushed
		}
		else//answering for request for server update about a chat with someone
		{
			//requesting for server update about a chat with someone
			Message serverResponse = Message(std::string("server update"), name, Helper::get_update_message_to_client(this->getChatContent(name,otherUsername), otherUsername, users), false);

			std::unique_lock<std::mutex> parseMessage(messageQueue);
			this->messages.push(std::pair<std::string, Message>(otherUsername, serverResponse));//pushing the message into the queue
			parseMessage.unlock();
			waitForMessage.notify_all();//alerting the thread that sends message that a message was pushed
		}
	}
	
}

/*
function that writes messages from file to queue and returns response to client
input: none
output: none
*/
void Server::parseMessagesFromQueueAndReturnResp()
{
	while (true)
	{
		std::unique_lock<std::mutex> parseMessage(messageQueue);
		waitForMessage.wait(parseMessage);//when a message is entered the thread starts to parse it
		this->writeMsgToFileAndSend();//keeping the function in the lock because it changes the queue
		parseMessage.unlock();
	}
}

/*
function that takes messages from queue, writes it if needed and sends it
input: none
output: none
*/
void Server::writeMsgToFileAndSend() 
{
	while (!this->messages.empty())//while there messages to deal with
	{
		std::pair<std::string, Message> nameAndMessage = this->messages.front();//getting the first message in the queue
		this->messages.pop();//removing the message we got from the queue

		std::string nameOfSecondUser = nameAndMessage.second.getNameOfSender();
		std::string nameOfReciver = nameAndMessage.second.getNameOfReciver();


		if (nameAndMessage.second.getMessageToOtherUserOrNot())//if the message was meant to other user writing it to file
		{
			std::ofstream outfile;
			std::string fileName;

			fileName = Helper::getFileName(nameAndMessage.second.getNameOfSender(), nameAndMessage.second.getNameOfReciver());//getting chat file between 2 users
			outfile.open(fileName, std::ios_base::app);
			outfile << nameAndMessage.second.getMsgContent();//writing message to file
			outfile.close();
			Helper::send_update_message_to_client(this->findSocketByName(nameOfReciver), this->getChatContent(nameOfSecondUser, nameOfReciver), nameOfSecondUser, this->getUsers());
		}
		else//returning serverUpdateResponse
		{

			Helper::sendData(this->findSocketByName(nameOfReciver), nameAndMessage.second.getMsgContent());
		}
	}
}

/*
function that updates the user list to all active users and accepts login
input: none
output: none
*/
void Server::updateUserListAndAcceptLogin()
{
	std::string serverUpdateMessage = std::to_string(MT_SERVER_UPDATE);
	serverUpdateMessage += "0000000";//len_cat_content = 0, len_par_username = 0
	std::string users = this->getUsers();
	serverUpdateMessage += Helper::getPaddedNumber(users.size(), LEN_ALL_USERS_LEN) + users;

	std::unique_lock<std::mutex> updateUsers(userMap);
	for (auto it = this->clientSessions.begin(); it != this->clientSessions.end(); it++)//sending to all the users the server update message
	{
		Message serverResponse(std::string("server update"), it->second, serverUpdateMessage, false);

		std::unique_lock<std::mutex> parseMessage(messageQueue);
		this->messages.push(std::pair<std::string, Message>(serverResponse.getNameOfReciver(), serverResponse));//pushing the message into the queue
		parseMessage.unlock();
	}
	updateUsers.unlock();
	waitForMessage.notify_all();//alerting the thread that sends message that a message was pushed
}

/*
function that finds a socket in the map by name
input: name of user to find its socket
output: socket of user
*/
const SOCKET Server::findSocketByName(const std::string& name) const
{
	std::unique_lock<std::mutex>searchingUser(userMap);
	for (auto it = this->clientSessions.begin(); it != this->clientSessions.end(); it++)
	{
		if (it->second == name)//if this function was called username must exist 
		{
			searchingUser.unlock();
			return it->first;
		}
		
	}
}

/*
function that checks if the there is still conection between client and server
input: client socket
output: if there is conection or not
*/
const bool Server::checkIfSocketConected(const SOCKET& clientSocket) const
{
	char buf;
	int err = recv(clientSocket, &buf, 1, MSG_PEEK);
	if (err == SOCKET_ERROR)
	{
		if (WSAGetLastError() != WSAEWOULDBLOCK)
		{
			return false;
		}
	}
	return true;
}