#pragma once
#include <iostream>

class Message
{
public:
	Message(const std::string &nameOfSender, const std::string& nameOfReciver, const std::string& msgContent,const bool messageToOtherUserOrNot);
	const std::string getNameOfSender() const;
	const std::string getNameOfReciver() const;
	const std::string getMsgContent() const;
	const bool getMessageToOtherUserOrNot() const;

private:
	std::string _nameOfSender;
	std::string _nameOfReciver;
	std::string _msgContent;
	bool _messageToOtherUserOrNot;//if true the message is meant to other user, if false it is a server response

};

